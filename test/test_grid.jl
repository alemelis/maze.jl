@testset "grid" begin
    @test_throws DomainError Maze.Grid(0,0)

    g = Maze.Grid(5,5)
    @test g.rows == 5
    @test g.cols == 5
    @test size(g.grid) == (5,5)
    @test size(g) == 25

    @test isnothing(g[-1,-1])
    @test ~isnothing(g[1,1])

    @test typeof(Maze.random_cell(g)) == Maze.Cell
end
