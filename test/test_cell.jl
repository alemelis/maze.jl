@testset "cell" begin
    a = Maze.Cell(1,1)
    @test a.id == "1,1"

    b = Maze.Cell(1,2)

    Maze.link!(a, b)
    @test haskey(a.links, b.id)
    @test haskey(b.links, a.id)
    @test length(Maze.links(a)) == length(Maze.links(b)) == 1

    Maze.unlink!(a, b)
    @test ~haskey(a.links, b.id)
    @test ~haskey(b.links, a.id)
    @test length(Maze.links(a)) == length(Maze.links(b)) == 0

    Maze.link!(a, b, false)
    @test haskey(a.links, b.id)
    @test ~haskey(b.links, a.id)
    @test length(Maze.links(a)) == 1
    @test length(Maze.links(b)) == 0

    @test Maze.neighbors(a) == []
end
