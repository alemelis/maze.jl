using Maze
using Test

@testset "Maze.jl" begin
    include("test_cell.jl")
    include("test_grid.jl")
end
