mutable struct Grid
    rows::Int64
    cols::Int64
    grid::Array{Cell,2}
end

function Grid(rows::Int64, cols::Int64)
    (rows<=0 || cols<=0) && throw(DomainError("rows and cols must be > 0"))
    grid = prepare_grid(rows, cols)
    g = Grid(rows, cols, grid)
    configure_cells!(g)
    g
end

function prepare_grid(rows::Int64, cols::Int64)
    grid = Array{Cell}(undef, rows, cols)
    for r=1:rows, c=1:cols
        grid[r,c] = Cell(r, c)
    end
    grid
end

Base.getindex(g::Grid, row::Int64, col::Int64) = 0<row≤g.rows&&0<col≤g.cols ? g.grid[row, col] : nothing

function configure_cells!(g::Grid)
    for i=1:g.rows, j=1:g.cols
        isnothing(g[i-1,j]) || (g[i,j].north = g[i-1,j].id)
        isnothing(g[i+1,j]) || (g[i,j].south = g[i+1,j].id)
        isnothing(g[i,j-1]) || (g[i,j].west = g[i,j-1].id)
        isnothing(g[i,j+1]) || (g[i,j].east = g[i,j+1].id)
    end
end

Base.size(g::Grid) = g.rows*g.cols

function random_cell(g::Grid)
    r = rand(1:g.rows)
    c = rand(1:g.cols)
    g[r,c]
end

# cell-by-cell iterator
Base.iterate(g::Grid, state=1) = state>size(g) ? nothing : (g.grid[state], state+1)

function Base.show(io::IO, g::Grid)
    body = " "^3
    corner = "+"
    wall = "|"
    output = [corner * ("-"^3*corner)^g.cols]
    for r=1:g.rows
        top = wall
        bottom = corner
        for c=1:g.cols
            cell = g[r,c]
            east = linked(cell, cell.east) ? " " : wall
            top *= body * east
            south = linked(cell, cell.south) ? body : "---"
            bottom *= south * corner
        end
        push!(output, top)
        push!(output, bottom)
    end
    println.(output)
end
