Base.@kwdef mutable struct Cell
    id::String = ""
    row::Int64
    col::Int64
    links::Dict{String,Bool} = Dict{String,Bool}()
    north::String = ""
    south::String = ""
    west::String = ""
    east::String = ""
end

Cell(row::Int64, col::Int64) = Cell(row=row, col=col, id="$(row),$(col)")

function link!(a::Cell, b::Cell, bidi=true)
    a.links[b.id] = true
    bidi && link!(b, a, false)
end

function unlink!(a::Cell, b::Cell, bidi=true)
    haskey(a.links, b.id) && delete!(a.links, b.id)
    bidi && unlink!(b, a, false)
end

links(c::Cell) = c.links |> keys

# neighbors(cell::Cell) = [id for id = [cell.north, cell.south, cell.west, cell.east] if id>""]

neighbors(c::Cell) = filter(>(""), [c.north, c.south, c.west, c.east])

linked(c::Cell, l::String) =  haskey(c.links, l)
