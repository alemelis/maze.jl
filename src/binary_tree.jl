function binary_tree!(g::Grid)
    for cell in g
        neighbors = []
        cell.north != "" && push!(neighbors, cell.north)
        cell.east != "" && push!(neighbors, cell.east)

        length(neighbors) == 0 && continue
        neighbor = rand(neighbors)
        i, j = parse.(Int,split(neighbor,","))
        link!(cell, g[i,j])
    end
end
